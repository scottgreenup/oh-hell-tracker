# BUGS
# Doesn't say dealer

import random
import sys
import time

import player

round = 10
players = []

currDealer = None

###############################################################################
def getPlayers():
	print "Please enter the name of the players on seperate lines:"

	name = raw_input()

	while name != "":
		person = player.Player();
		person.setName(name)
		person.setBet(0)
		person.setScore(0)

		name = raw_input()

		players.append(person)

	if len(players) == 0:
		sys.exit()

###############################################################################
def getBets():
	print "Time for bets"

	start = players.index(dealer) + 1

	for i in range(start, start + len(players)):
		index = i % len(players)

		print "\t%s's bet: " % (players[index].name),
		bet = raw_input()

		players[index].bet = bet

###############################################################################
def getHands():
	print "Time to calculate scores"

	start = players.index(dealer) + 1

	for i in range(start, start + len(players)):
		index = i % len(players)

		print "\tHow many hands did %s win?:\t" % players[index].name,
		hands = raw_input()

		if (hands == players[index].bet):
			players[index].score += 10

		players[index].score += int(hands)

###############################################################################
def sortList(pp):
	pivotIndex = random.randint(0, len(players) - 1)
	pivot = players[pivotIndex]

	less = []
	greater = []

	for x in pp:

		if (x == pivot):
			continue

		if x.score <= pivot.score:
			less.append(x)
		else:
			greater.append(x)

	if len(less) <= 1 and len(greater) <= 1:
		return greater + [pivot] + less
	elif len(less) <= 1:
		return sortList(greater) + [pivot] + less
	elif len(greater) <= 1:
		return greater + [pivot] + sortList(less) 
	else:
		return sortList(greater) + [pivot] + sortList(less)
		

###############################################################################
if __name__ == "__main__":

	getPlayers()

	# randomly select dealer
	random.seed(time.time())
	dealer = players[random.randint(0, len(players) - 1)]

	# main game loop
	while round != 0:
		print "############################################################"
		print "Round:\t %d" % (round)
		print "Dealer:\t %s" % (dealer.name)
		print "Please deal %d cards!" % (round)
		print ""

		getBets()
		print ""

		# show bets
		# print "\n"
		# print "ROUND %d bets are: " % round
		# for i in range(0, len(players)):
		# 	print "\t%s\t%s" % (players[i], bets[i])


		getHands()
		print ""

		# show scores
		print "Round %d over, scores are: " % round

		# linear search for max score
		
		pp = []

		for p in players:
			pp.append(p)

		pp = sortList(pp)

		first = True

		for p in enumerate(pp):
			if first:
				first = False
				print "\t%d. %s has score %d" % (p[0] + 1, p[1].name, p[1].score)

			else:
				print "\t%d. %s has score %d (-%d)" % (p[0] + 1, p[1].name, p[1].score, pp[0].score - p[1].score)

		# sort pp
		# display scores in order
		# display difference from leader

		#gap = len(list)		

		# update dealer		
		dealerIndex = (players.index(dealer) + 1) % len(players)


		round = round - 1;
		print ""














